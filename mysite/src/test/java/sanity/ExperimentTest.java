package sanity;

import app.User.User;
import app.User.UserDbHelper;
import app.UserApiHelper;
import com.github.javafaker.Faker;
import org.testng.annotations.Test;
import report.TestReport;

public class ExperimentTest {

    @Test(groups = "experiments")
    public void experimentTest() {
        TestReport.info("Test case starting");
        TestReport.error("Error...");
        TestReport.debug("Debug...");
    }

    @Test
    public void fakeUserTest() {
        Faker faker = new Faker();

        System.out.println(faker.name().firstName());
        System.out.println(faker.name().lastName());
        System.out.println(faker.name().fullName());
        System.out.println(faker.name().username());

        System.out.println(faker.address().city());
        System.out.println(faker.address().country());
        System.out.println(faker.address().fullAddress());
    }

    @Test
    public void findUserByUsername() {
        User user = new User();
        user.setUsername("user");
        UserApiHelper.findUserInResponse(user);
    }

    @Test
    public void createUserObjectFrom() {
        UserDbHelper.createUserObjectByUsername("user");
    }
}
