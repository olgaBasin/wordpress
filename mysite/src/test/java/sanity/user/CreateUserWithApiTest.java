package sanity.user;

import api.ApiClient;
import app.User.User;
import app.UserApiHelper;
import common.RandomDataGenerator;
import org.testng.Assert;
import org.testng.annotations.Test;
import report.TestReport;

public class CreateUserWithApiTest {

    @Test(groups = "sanity")
    public void createUserTest() {
        TestReport.info("=====> Generating random user");
        User user = RandomDataGenerator.generateFakeUser();
        TestReport.info("New user info: " + user.toString());
        TestReport.info("=====> Sending request to create new User");
        ApiClient.sendUserPostRequest(user.newUserRequestAsString());
        TestReport.info("=====> Verifying data in response");
        Assert.assertTrue(ApiClient.verifyResponseCode(201));
        Assert.assertTrue(ApiClient.verifyFieldInResponse("username", user.getUsername()));
        Assert.assertTrue(UserApiHelper.findUserInResponse(user));

        // Check record in DataBase
        // Check user in GUI
//        LoginPage.loginToWordpress();
//        DashboardPage.navigateToUserPage();
//        UserPage.isOnPage();
//        UserPage.findUserInUserTable("user8");
    }
}
