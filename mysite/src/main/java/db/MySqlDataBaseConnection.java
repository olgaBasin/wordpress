package db;

import utils.PropertiesLoader;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class MySqlDataBaseConnection {

    private static Connection conn;
    private static String path = "src/main/resources/connectionDB.properties";
    private static String delimiter = ",";

    public static Connection connectToDb() {
        try {
            Properties prop = PropertiesLoader.load(path);
            String host = prop.getProperty("host");
            String username = prop.getProperty("username");
            String password = prop.getProperty("password");

            //           List<String> list = Parser.parseCSV(path, delimiter);
            //           conn = DriverManager.getConnection(list.get(0), list.get(1), list.get(2));

            conn = DriverManager.getConnection(host, username, password);
        } catch (SQLException e) {
            e.getMessage();

//        } catch (IOException e) {
//            e.printStackTrace();
        }
        return conn;
    }
}
