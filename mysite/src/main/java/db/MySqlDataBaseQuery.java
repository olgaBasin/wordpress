package db;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MySqlDataBaseQuery {

    private static PreparedStatement preparedStatement;
    private static ResultSet resultSet;

    public static ResultSet fetchResults(String query) {

        Connection conn = MySqlDataBaseConnection.connectToDb();
        try {
            preparedStatement = conn.prepareStatement(query);
            resultSet = preparedStatement.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return resultSet;
    }

    public static List<Map> getResultsAsMap(ResultSet rs) {

        List<Map> list = new ArrayList<Map>();
        try {
            ResultSetMetaData md = rs.getMetaData();
            int columns = md.getColumnCount();
            while (rs.next()) {
                HashMap row = new HashMap(columns);
                for (int i = 1; i <= columns; ++i) {
                    row.put(md.getColumnName(i), rs.getObject(i));
                }
                list.add(row);
            }
        } catch (SQLException e) {
            e.getMessage();
        }
        return list;
    }

    public static void getResultByQuery(String getAllUsers) {
    }
}
