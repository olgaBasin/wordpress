package db;

import java.sql.ResultSet;
import java.util.List;
import java.util.Map;

public class Main {
    public static String SQL_SELECT_POSTS = "SELECT * FROM wp_posts;";
    public static String SQL_SELECT_USERS = "SELECT * FROM wp_users;";
    public static String SQL_SELECT_USER_NAME = "SELECT user_nicename FROM wp_users;";

    public static void main(String[] args) {

        ResultSet rs_posts = MySqlDataBaseQuery.fetchResults(SQL_SELECT_POSTS);
        List<Map> posts = MySqlDataBaseQuery.getResultsAsMap(rs_posts);

        ResultSet rs_user_name = MySqlDataBaseQuery.fetchResults(SQL_SELECT_USER_NAME);
        List<Map> user_name = MySqlDataBaseQuery.getResultsAsMap(rs_user_name);
        for (Map curr : user_name) {
            String niceName = (String) curr.get("user_nicename");
            System.out.println(niceName);
        }
    }
}
