package common;

import app.User.User;
import com.github.javafaker.Faker;
import com.google.common.base.Charsets;
import com.google.common.io.Resources;
import com.hubspot.jinjava.Jinjava;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;


public class RandomDataGenerator {

    public static void main(String[] args) {
        generateNewUserRequestFromTemplateFile("");
    }

    public static String generateNewUserRequestFromTemplateFile(String pathToDataFile) {
        User user = generateFakeUser();
        Jinjava jinjava = new Jinjava();
        Map<String, Object> context = new HashMap<>();
        context.put("username", user.getUsername());
        context.put("name", user.getUsername());
        context.put("first_name", user.getFirstName());
        context.put("last_name", user.getLastName());
        context.put("email", user.getEmail());
        context.put("url", "");
        context.put("description", "");
        context.put("locale", "en_US");
        context.put("nickname", user.getUsername());
        context.put("role", user.getRoles());
        context.put("password", user.getPassword());

        String template = null;
        try {
            template = Resources.toString(Resources.getResource("user-context.json.j2"), Charsets.UTF_8);
        } catch (IOException e) {
            e.getMessage();
        }

        String renderedTemplate = jinjava.render(template, context);
        return renderedTemplate;
    }

    public static User generateFakeUser() {
        User user = new User();
        Faker faker = new Faker();
        String firstName = faker.name().firstName();
        String lastName = faker.name().lastName();

        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setEmail(String.format("%s.%s@mail.com", firstName.toLowerCase(), lastName.toLowerCase()));
        user.setPassword("password");
        user.setUsername(String.format("%s.%s", firstName.toLowerCase(), lastName.toLowerCase()));
        user.setRoles("subscriber");
        return user;
    }
}
