package api;

import okhttp3.*;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class ApiClient {

    private static final Logger logger = LoggerFactory.getLogger(ApiClient.class);

    private static final String BASE_URL = "http://192.168.247.5:8080/wp-json/wp/v2";
    private static final OkHttpClient httpClient = new OkHttpClient();
    private static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");
    protected static Response response;
    protected static String responseBodyString;

    public static void sendGet(String endPoint) throws IOException {
        logger.debug("Preparing request to RESTApi {}/users", BASE_URL);

        Request request = new Request.Builder()
                .url(BASE_URL + endPoint)
                .addHeader("Authorization", Credentials.basic("olgaBasin", "12358"))
                .build();

        logger.debug("Executing request");

        response = httpClient.newCall(request).execute();
        responseBodyString = response.body().string();

        logger.info("Response code from resource {}", response.code());
        logger.info("Response body string: {}", responseBodyString);

//        Headers responseHeaders = response.headers();
//        for (int i = 0; i < responseHeaders.size(); i++) {
//            logger.info("Header {} : {}", responseHeaders.name(i), responseHeaders.value(i));
//        }
//
//        logger.info("Response content: {}", response.body().string());
    }

    public static void sendPOST(String endPoint, String jsonBody) throws IOException {

        RequestBody body = RequestBody.create(jsonBody, JSON);

        Request request = new Request.Builder()
                .url(BASE_URL + endPoint)
                .addHeader("User-Agent", "OkHttp Bot")
                .addHeader("Authorization", Credentials.basic("olgaBasin", "12358"))
                .post(body)
                .build();

        response = httpClient.newCall(request).execute();
        responseBodyString = response.body().string();

        logger.info("Response code from resource {}", response.code());
        logger.info("Response body string: {}", responseBodyString);
    }

    public static Response sendUserGetRequest() {
        logger.debug("Sending GET request to {}{} ", BASE_URL, ApiEndPoints.users);
        try {
            sendGet(ApiEndPoints.users);
        } catch (IOException e) {
            logger.error("Failed to send POST request to {}{}} API", BASE_URL, ApiEndPoints.users);
            e.getMessage();
        }
        return response;
    }

    public static Response sendUserPostRequest(String requestAsString) {
        logger.debug("Sending POST request to {}{} ", BASE_URL, ApiEndPoints.users);
        try {
            sendPOST(ApiEndPoints.users, requestAsString);
        } catch (IOException e) {
            logger.error("Failed to send POST request to {}{}} API", BASE_URL, ApiEndPoints.users);
            e.getMessage();
        }
        return response;
    }

    public static boolean verifyResponseCode(int expectedResponseCode) {
        return expectedResponseCode == response.code();
    }

    public static boolean verifyFieldInResponse(String responseKey, String expectedValue) {
        JSONObject responseBodyAsJSON = new JSONObject(responseBodyString);
        return expectedValue.equals(responseBodyAsJSON.get(responseKey));
    }
}
