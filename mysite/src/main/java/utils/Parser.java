package utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class Parser {

    public static List<String> parseCSV(String path, String delimiter) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(new File(path)));
        String line = reader.readLine();
        List<String> listArray = null;

        while (line != null) {
            String[] split = line.split(delimiter);
            listArray = Arrays.asList(split);
            line = reader.readLine();
        }
        return listArray;
    }
}
