package app.User;

public class UserQueries {
    public static final String GET_ALL_USERS = "SELECT DISTINCT\n" +
            "\tu.user_login AS \"username\", \n" +
            "\tu.user_email AS \"email\", \n" +
            "\tfn.first_name AS \"firstName\", \n" +
            "\tln.last_name AS \"lastName\",\n" +
            "\tu.user_url AS \"website\",\n" +
            "\tu.user_pass AS \"password\",\n" +
            "\tr.user_role AS \"role\"\n" +
            "\tFROM wordpress.wp_users u\n" +
            "\tJOIN\n" +
            "\t(\n" +
            "\t\tSELECT user_id, meta_value AS \"first_name\"\n" +
            "\t\tFROM wp_usermeta\n" +
            "\t\tWHERE meta_key = \"first_name\"\n" +
            "\t) AS fn\n" +
            "\tON fn.user_id = u.ID\n" +
            "\tJOIN\n" +
            "\t(\n" +
            "\t\tSELECT user_id, meta_value AS \"last_name\"\n" +
            "\t\tFROM wp_usermeta\n" +
            "\t\tWHERE meta_key = \"last_name\"\n" +
            "\t) AS ln\n" +
            "\tON ln.user_id = u.ID\n" +
            "\tJOIN\n" +
            "\t(\n" +
            "\t\tSELECT user_id, meta_value AS \"user_role\"\n" +
            "\t\tFROM wp_usermeta\n" +
            "\t\tWHERE meta_key = \"wp_capabilities\"\n" +
            "\t) AS r\n" +
            "\tON r.user_id = u.ID";
}
