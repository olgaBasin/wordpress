package app.User;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class User {

    private String username;
    private String email;
    private String firstName;
    private String lastName;
    private String webSite;
    private String password;
    private Boolean sendUserNotification;
    private String role;
    private List<String> roles = new ArrayList<>();

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getWebSite() {
        return webSite;
    }

    public void setWebSite(String webSite) {
        this.webSite = webSite;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Boolean getSendUserNotification() {
        return sendUserNotification;
    }

    public void setSendUserNotification(Boolean sendUserNotification) {
        this.sendUserNotification = sendUserNotification;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public void setRoles(String... roles) {
        this.roles = Arrays.asList(roles);
    }

    public List<String> getRoles() {
        return roles;
    }

    public String printRoles() {
        StringBuffer stringBuffer = new StringBuffer();
        for (String role : roles) {
            stringBuffer.append(role);
            stringBuffer.append(",");
        }
        return stringBuffer.toString().substring(0, stringBuffer.length() - 1);
    }

    public JSONObject newUserRequest() {
        JSONObject userObject = new JSONObject();
        userObject.put("username", getUsername());
        userObject.put("name", getUsername());
        userObject.put("first_name", getFirstName());
        userObject.put("last_name", getLastName());
        userObject.put("email", getEmail());
        userObject.put("url", "");
        userObject.put("description", "");
        userObject.put("locale", "en_US");
        userObject.put("nickname", getUsername());
        userObject.put("role", getRoles());
        userObject.put("password", getPassword());
        return userObject;
    }

    public String newUserRequestAsString() {
        return newUserRequest().toString();
    }

    @Override
    public String toString() {
        return String.format("Username: %s|First Name:%s|Last Name: %s|Email: %s|Roles: %s", getUsername(), getFirstName(), getLastName(), getEmail(), printRoles());
    }
}
