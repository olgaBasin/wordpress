package app;

import api.ApiClient;
import app.User.User;
import org.json.JSONArray;
import org.json.JSONObject;

public class UserApiHelper extends ApiClient {
    public static boolean findUserInResponse(String username, User user) {
        boolean result = false;
        ApiClient.sendUserGetRequest();
        JSONArray jsonArray = new JSONArray(responseBodyString);

        for(int i= 0; i < jsonArray.length(); i++) {
            JSONObject jsonObject = jsonArray.getJSONObject(i);
            if(jsonObject.get(username).equals(user.getUsername())) {
                result = true;
            }
        }
        return result;
    }

    public static boolean findUserInResponse(User user) {
        return findUserInResponse("name", user);
    }
}
