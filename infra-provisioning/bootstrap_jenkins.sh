#!/bin/bash

# Install docker from Docker-ce repository
echo "[ Installing docker container engine ]"
yum install -y -q yum-utils device-mapper-persistent-data lvm2 > /dev/null 2>&1
yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo > /dev/null 2>&1
yum install -y -q docker-ce >/dev/null 2>&1

# Enable docker service
echo "[ Enabling and start docker service ]"
systemctl enable docker >/dev/null 2>&1
systemctl start docker
usermod -aG docker vagrant
newgrp docker


## Enable ssh password authentication
echo "[ Enabling ssh password authentication ]"
sed -i 's/^PasswordAuthentication no/PasswordAuthentication yes/' /etc/ssh/sshd_config
systemctl reload sshd

# Set Root password
echo "[ Setting root password ]"
echo "vagrantroot" | passwd --stdin root >/dev/null 2>&1

# [ Updating vagrant user's bashrc file ]
echo "export TERM=xterm" >> /etc/bashrc

# Installing docker-compose
echo [ Installing docker-compose ]

curl -L "https://github.com/docker/compose/releases/download/1.25.5/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose
ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose


# Preparing Wordpress docker-compose.yaml
echo "[ Preparing Wordpress docker-compose.yaml ]"

mkdir /home/vagrant/jenkins

cat >>/home/vagrant/jenkins/docker-compose.yaml<<EOF
---
version: '3'
services:
  jenkins:
    image: 'jenkins/jenkins:lts'
    ports:
      - '8080:8080'
      - '50000:50000'
    volumes:
      - 'jenkins_data:/var/jenkins_home'
volumes:
  jenkins_data:
    driver: local
EOF

# Creating Wordpress Service

echo [ Creating Wordpress Service and reloading ]

cat >>/etc/systemd/system/jenkins_start.service<<EOF
[Unit]
Description=Jenkins Service
After=network.target

[Service]
Type=simple
User=vagrant
ExecStart=/usr/local/bin/docker-compose -f /home/vagrant/jenkins/docker-compose.yaml up -d
Restart=on-abort

[Install]
WantedBy=multi-user.target
EOF

chmod 664 /etc/systemd/system/jenkins_start.service

systemctl daemon-reload
systemctl enable jenkins_start.service >/dev/null 2>&1
systemctl start jenkins_start.service